galleryhtml
===========

Python to make thumbnails and generate HTML and/or JSON pointing to them.

Implemented as a badly-designed class with a command-line wrapper.  Probably
wrong in many particulars.  I don't know Python.

As of September 2016, this is just kind of an experiment.  I'll probably change
the interface (and might also abandon the whole thing in favor of some existing
tool).  If I get it to a place where I think I won't break the interface and
there are some unit tests in place, that will probably be a v1.0.0.

rationale
---------

I just want to make some simple photo galleries for my website, using something
I can easily plumb into Makefiles or [Vim macros][vim-macros].

synopsis
--------

```sh
gallery-html --base_url=https://example.com/ --base_dir=. -x=256 -y=256
```

installing
----------

```sh
# I needed this on my Debian Jessie system:
# sudo apt-get install python3-pil
git clone https://github.com/brennen/galleryhtml
cd galleryhtml
sudo python3 setup.py install
```

Installing for development:

```sh
sudo pip3 install -e
```

gallery-html usage
------------------

<!-- exec -->

    $ gallery-html --help
    gallery-html - generate thumbnails and print html for images
    
    Usage:
      gallery-html [--source <pattern>...] [--output <dir>] [--base_url <url>] [-x <n>] [-y <n>] [--json <filename>] [--html <filename>] [--overwrite]
      gallery-html (-h | --help)
      gallery-html (-v | --version)
    
    Options:
      --source <pattern>  Source directory or image file(s) to make gallery from
      --output <dir>      Parent directory on local filesystem for thumbnails
      --base_url <url>    Base URL for images
      -x <n>              Maximum width of thumbnails in pixels
      -y <n>              Maximum height of thumbnails in pixels
      --json <filename>   Render JSON output to filename
      --html <filename>   Render HTML output to filename
      --overwrite         Replace existing thumbnail files
      -h --help           Show this screen
      -v --version        Display version

<!-- end -->

[vim-macros]: https://github.com/brennen/bpb-kit/blob/master/home/bin/vim-filter-exec-raw
