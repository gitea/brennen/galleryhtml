from setuptools import setup

setup(name='galleryhtml',
      version='0.8.1',
      description='Generate thumbnails for images and print a simple HTML gallery',
      url='http://github.com/brennen/galleryhtml',
      author='Brennen Bearnes',
      author_email='bbearnes@gmail.com',
      license='GPL',
      packages=['galleryhtml'],
      install_requires=[
          'dominate',
          'Pillow',
          'docopt'
      ],
      scripts=['bin/gallery-html'],
      zip_safe=False)
