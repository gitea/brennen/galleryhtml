#!/usr/bin/env bash

# This probably isn't necessary:
sudo pip3 install -e .

cd test

if [[ -d Thumbs ]]; then
  echo "deleting any existing thumbs"
  rm -rf Thumbs
fi

if [[ -f index.html ]]; then
  echo "deleting index.html"
  rm index.html
fi

if [[ -f index.json ]]; then
  echo "deleting index.json"
  rm index.json
fi

gallery-html --base_url "https://p1k3.com/photos/example/" --html index.html
tail -3 index.html
echo
ls Thumbs

gallery-html --json index.json
tail -3 index.json
echo

gallery-html --overwrite -x 500 -y 500
ls Thumbs
